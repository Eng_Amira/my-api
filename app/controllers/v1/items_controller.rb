module V1
  class ItemsController < ApplicationController
        before_action :set_todo
        before_action :set_todo_item, only: [:show, :update, :destroy]
      
  # GET /todos/:todo_id/items
  #=begin
  # @api {get} /items/ List all Items
  # @apiVersion 0.3.0
  # @apiName GetItem
  # @apiGroup Item
  # @apiPermission user
  #
  # @apiDescription get details of all items.
  #
  #
  # @apiExample Example usage:
  # curl -H "Authorization: eyJh......Ylg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X GET http://localhost:3000/todos/8/items
  #
  # @apiSuccess {Integer}    id               Items-ID.
  # @apiSuccess {String}     name             Items name
  # @apiSuccess {String}     done             Items status
  # @apiSuccess {String}     todo_id          Items todo_id
  # @apiSuccess {Datetime}   created_at       date of creating the Items.
  # @apiSuccess {Datetime}   updated_at       date of updating the Items.
  #
  # @apiError NoAccessRight Only authenticated users can access the data.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 400 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
  #=end
    def index
      json_response(@todo.items)
    end
      
  # GET /todos/:todo_id/items/:id

  #=begin
  # @api {get} /todos/:todo_id/:id Read data of an item
  # @apiVersion 0.3.0
  # @apiName GetItem
  # @apiGroup Item
  # @apiPermission user
  #
  # @apiDescription get details of an item.
  #
  # @apiParam {Integer} id The Item-ID.
  #
  # @apiExample Example usage:
  # curl -H "Authorization: eyJh.....lg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X GET http://localhost:3000/todos/8/items/3
  #
  # @apiSuccess {Integer}    id               Item-ID.
  # @apiSuccess {String}     name             Item name
  # @apiSuccess {String}     done             Item status
  # @apiSuccess {String}     todo_id          Item todo_id
  # @apiSuccess {Datetime}   created_at       date of creating the Item.
  # @apiSuccess {Datetime}   updated_at       date of updating the Item.
  #
  # @apiError NoAccessRight Only authenticated users can access the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.
  #
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "ItemNotFound"
  #    }
  # @apiErrorExample Response (example):
  #     HTTP/1.1 400 Bad Request
  #     {
  #       "error": "Missing token"
  #    }
#=end
    def show
      json_response(@item)
    end
      
  # POST /todos/:todo_id/items

  #=begin
  # @api {post} /todos/:todo_id/items Create new Item for a Todo
  # @apiVersion 0.3.0
  # @apiName postItem
  # @apiGroup Item
  # @apiPermission user
  # @apiDescription create new item.
  # @apiParam {String}     name         The Item name
  #
  # @apiExample Example usage:
  # curl -d "name=addnewitem" -H "Authorization: eyJh......Ylg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X POST http://localhost:3000/todos/8/items
  #
  # @apiSuccess {Integer}    id               Item-ID.
  # @apiSuccess {String}     name             Item name
  # @apiSuccess {String}     done             Item status
  # @apiSuccess {String}     todo_id          Item todo_id
  # @apiSuccess {Datetime}   created_at       date of creating the Item.
  # @apiSuccess {Datetime}   updated_at       date of updating the Item
  #
  # @apiError NoAccessRight Only authenticated users can create Items.
  #
  # @apiError MissingToken invalid-token.
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 400 Bad Request
  #     {
  #       "error": "Missing Token"
  #    }
  #=end
        
    def create
      @todo.items.create!(item_params)
      json_response(@todo, :created)
    end
      
  # PUT /todos/:todo_id/items/:id
    #=begin
  # @api {put} /todos/:todo_id/items/:id  Update data of an Item
  # @apiVersion 0.3.0
  # @apiName PutItem
  # @apiGroup Item
  # @apiPermission user
  #
  # @apiDescription update details of a Item.
  #
  # @apiParam {String} name The Item-name.
  #
  # @apiExample Example usage:
  # curl -d "name=changed" -H "Authorization: eyJhb....Ylg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X PUT http://localhost:3000/todos/8/items/4
  #
  # @apiSuccess {Integer}    id               Item-ID.
  # @apiSuccess {String}     name             Item name
  # @apiSuccess {String}     done             Item status
  # @apiSuccess {String}     todo_id          Item todo_id
  # @apiSuccess {Datetime}   created_at       date of creating the Item.
  # @apiSuccess {Datetime}   updated_at       date of updating the Item
  #
  # @apiError NoAccessRight Only authenticated users can update the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.
  #
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "ItemNotFound"
  #    }
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
#=end
    def update
      @item.update(item_params)
      head :no_content
    end
      
  # DELETE /todos/:todo_id/items/:id

  #=begin
  # @api {delete} /todos/:todo_id/items/:id Delete an Item
  # @apiVersion 0.3.0
  # @apiName DeleteItem
  # @apiGroup Item
  # @apiPermission user
  #
  # @apiDescription Delete an item.
  #
  # @apiExample Example usage:
  # curl -H "Authorization: eyJhb....Ylg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X DELETE http://localhost:3000/todos/8/items/4
  #
  # @apiSuccessExample Response (example):
  #     HTTP/ 200 Success
  #     {
  #       "success": "200 ok"
  #    }
  # @apiError NoAccessRight Only authenticated users can delete the data.
  # @apiError ItemNotFound   The <code>id</code> of the Item was not found.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "ItemNotFound"
  #    }
  #=end
    def destroy
      @item.destroy
      head :no_content
    end
      
  private
      
    def item_params
      params.permit(:name, :done)
    end
      
    def set_todo
      @todo = Todo.find(params[:todo_id])
    end
      
    def set_todo_item
      @item = @todo.items.find_by!(id: params[:id]) if @todo
    end
end
end
