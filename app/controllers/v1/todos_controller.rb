module V1
  class TodosController < ApplicationController
    before_action :set_todo, only: [:show, :update, :destroy]

  #=begin
  # @api {get} /todos/ List all Todos
  # @apiVersion 0.3.0
  # @apiName GetTodo
  # @apiGroup Todo
  # @apiPermission user
  #
  # @apiDescription get details of all todos.
  #
  #
  # @apiExample Example usage:
  # curl -H "Authorization: eyJh.......lg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X GET http://localhost:3000/todos
  #
  # @apiSuccess {Integer}    id               Todos-ID.
  # @apiSuccess {String}     title            Todo title
  # @apiSuccess {String}     item             Todo items
  # @apiSuccess {String}     created_by       Todo creator
  # @apiSuccess {Datetime}   created_at       date of creating the Todo.
  # @apiSuccess {Datetime}   updated_at       date of updating the Todo.
  #
  # @apiError NoAccessRight Only authenticated users can access the data.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 400 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
  #=end
  # GET /todos
  def index
    # get current user todos
    @todos = current_user.todos.paginate(page: params[:page], per_page: 20)
    json_response(@todos)
  end

  # POST /todos
  #=begin
  # @api {post} /todos/ Create new Todo
  # @apiVersion 0.3.0
  # @apiName postTodo
  # @apiGroup Todo
  # @apiPermission user
  # @apiDescription create new todo.
  # @apiParam {String}     title         The Todo title
  #
  # @apiExample Example usage:
  # curl -d "title=addtodonew" -H "Authorization: eyJh......Ylg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X POST http://localhost:3000/todos
  #
  # @apiSuccess {Integer}    id               Todos-ID.
  # @apiSuccess {String}     title            Todo title
  # @apiSuccess {String}     created_by       Todo creator
  # @apiSuccess {Datetime}   created_at       date of creating the Todo.
  # @apiSuccess {Datetime}   updated_at       date of updating the Todo.
  #
  # @apiError NoAccessRight Only authenticated users can createcTodo.
  #
  # @apiError MissingToken invalid-token.
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 400 Bad Request
  #     {
  #       "error": "Missing Token"
  #    }
  #=end
  def create
    # create todos belonging to current user
    @todo = current_user.todos.create!(todo_params)
    json_response(@todo, :created)
  end

  # GET /todos/:id
  #=begin
  # @api {get} /todos/:id Read data of a todo
  # @apiVersion 0.3.0
  # @apiName GetTodo
  # @apiGroup Todo
  # @apiPermission admin
  #
  # @apiDescription get details of a todo.
  #
  # @apiParam {Integer} id The Todo-ID.
  #
  # @apiExample Example usage:
  # curl -H "Authorization: eyJh.....lg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X GET http://localhost:3000/todos/200
  #
  # @apiSuccess {Integer}    id               Todos-ID.
  # @apiSuccess {String}     title            Todo title
  # @apiSuccess {String}     created_by       Todo creator
  # @apiSuccess {Datetime}   created_at       date of creating the Todo.
  # @apiSuccess {Datetime}   updated_at       date of updating the Todo.
  #
  # @apiError NoAccessRight Only authenticated users can access the data.
  # @apiError TodoNotFound   The <code>id</code> of the Todo was not found.
  #
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "TodoNotFound"
  #    }
  # @apiErrorExample Response (example):
  #     HTTP/1.1 400 Bad Request
  #     {
  #       "error": "Missing token"
  #    }
#=end
  def show
    json_response(@todo)
  end

  # PUT /todos/:id
  #=begin
  # @api {put} /todos/:id/edit Update data of a Todo
  # @apiVersion 0.3.0
  # @apiName PutTodo
  # @apiGroup Todo
  # @apiPermission user
  #
  # @apiDescription update details of a Todo.
  #
  # @apiParam {String} title The Todo-title.
  #
  # @apiExample Example usage:
  # curl -d "title=changed" -H "Authorization: eyJhb....Ylg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X PUT http://localhost:3000/todos/200
  #
  # @apiSuccess {Integer}    id               Todos-ID.
  # @apiSuccess {String}     title            Todo title
  # @apiSuccess {String}     created_by       Todo creator
  # @apiSuccess {Datetime}   created_at       date of creating the Todo.
  # @apiSuccess {Datetime}   updated_at       date of updating the Todo.
  #
  # @apiError NoAccessRight Only authenticated users can update the data.
  # @apiError TodoNotFound   The <code>id</code> of the Todo was not found.
  #
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "TodoNotFound"
  #    }
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
#=end
  def update
    @todo.update(todo_params)
    head :no_content
  end

  # DELETE /todos/:id
  #=begin
  # @api {delete} /todos/:id Delete a Todo
  # @apiVersion 0.3.0
  # @apiName DeleteTodo
  # @apiGroup Todo
  # @apiPermission user
  #
  # @apiDescription Delete a todo.
  #
  # @apiExample Example usage:
  # curl -H "Authorization: eyJhb....Ylg3Y" -H "Content-Type: application/x-www-form-urlencoded" -X DELETE http://localhost:3000/todos/200
  #
  # @apiSuccessExample Response (example):
  #     HTTP/ 200 Success
  #     {
  #       "success": "200 ok"
  #    }
  # @apiError NoAccessRight Only authenticated users can delete the data.
  # @apiError TodoNotFound   The <code>id</code> of the Todo was not found.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Not Authenticated
  #     {
  #       "error": "NoAccessRight"
  #    }
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "TodoNotFound"
  #    }
  #=end
  def destroy
    @todo.destroy
    head :no_content
  end

  private

  def todo_params
    # whitelist params
    params.permit(:title)
  end

  def set_todo
    @todo = Todo.find(params[:id])
  end
end
end
