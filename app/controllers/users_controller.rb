#=begin
  # @api {post} /users/ Create new User  
  # @apiVersion 0.3.0
  # @apiName postUser
  # @apiGroup User
  # @apiPermission user
  #
  # @apiDescription create new user.
  #
  # @apiParam {String}     name	         The User name
  # @apiParam {String}     email	       The User email
  # @apiParam {String}     password	     The User password
  #
  # @apiExample Example usage:
  # curl -d "name=test&email=test@email.com&password=test&password_confirmation=test" -H "Content-Type: application/x-www-form-urlencoded" -X POST http://localhost:3000/signup  
  #
  # @apiSuccess {Integer}   id            The Users-ID.
  # @apiSuccess {String}     name         The User name
  # @apiSuccess {String}     email         The User email
  # @apiSuccess {String}     password     The User password
  # @apiSuccess {Datetime}   created_at    date of creating the user.
  # @apiSuccess {Datetime}   updated_at    date of updating the user.
  #
  # @apiSuccessExample Success-Response:
  #     HTTP/1.1 200 OK
  #     {
  #       "message": "Account created successfully"
  #    }
  # @apiError Validation failed: Password can't be blank, Name can't be blank, Email can't be blank, Password digest can't be blank.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 422 Unprocessable Entity
  #     {
  #       "message": "Validation failed: Password can't be blank, Password digest can't be blank"
  #    }
#=end
class UsersController < ApplicationController
    skip_before_action :authorize_request, only: :create
    # POST /signup
  # return authenticated token upon signup
  def create
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

  private

  def user_params
    params.permit(
      :name,
      :email,
      :password,
      :password_confirmation
    )
  end

end
