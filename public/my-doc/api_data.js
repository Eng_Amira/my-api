define({ "api": [
  {
    "type": "delete",
    "url": "/todos/:todo_id/items/:id",
    "title": "Delete an Item",
    "version": "0.3.0",
    "name": "DeleteItem",
    "group": "Item",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>Delete an item.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhb....Ylg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X DELETE http://localhost:3000/todos/8/items/4",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 Success\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can delete the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"ItemNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/items_controller.rb",
    "groupTitle": "Item"
  },
  {
    "type": "get",
    "url": "/todos/:todo_id/:id",
    "title": "Read data of an item",
    "version": "0.3.0",
    "name": "GetItem",
    "group": "Item",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>get details of an item.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>The Item-ID.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJh.....lg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X GET http://localhost:3000/todos/8/items/3",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Item-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Item name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "done",
            "description": "<p>Item status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Item todo_id</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Item.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Item.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"ItemNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/items_controller.rb",
    "groupTitle": "Item"
  },
  {
    "type": "get",
    "url": "/items/",
    "title": "List all Items",
    "version": "0.3.0",
    "name": "GetItem",
    "group": "Item",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>get details of all items.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJh......Ylg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X GET http://localhost:3000/todos/8/items",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Items-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Items name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "done",
            "description": "<p>Items status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Items todo_id</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Items.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Items.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/items_controller.rb",
    "groupTitle": "Item"
  },
  {
    "type": "put",
    "url": "/todos/:todo_id/items/:id",
    "title": "Update data of an Item",
    "version": "0.3.0",
    "name": "PutItem",
    "group": "Item",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>update details of a Item.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The Item-name.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -d \"name=changed\" -H \"Authorization: eyJhb....Ylg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X PUT http://localhost:3000/todos/8/items/4",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Item-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Item name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "done",
            "description": "<p>Item status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Item todo_id</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Item.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Item</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can update the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"ItemNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/items_controller.rb",
    "groupTitle": "Item"
  },
  {
    "type": "post",
    "url": "/todos/:todo_id/items",
    "title": "Create new Item for a Todo",
    "version": "0.3.0",
    "name": "postItem",
    "group": "Item",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>create new item.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The Item name</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -d \"name=addnewitem\" -H \"Authorization: eyJh......Ylg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X POST http://localhost:3000/todos/8/items",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Item-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Item name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "done",
            "description": "<p>Item status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Item todo_id</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Item.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Item</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can create Items.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid-token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/items_controller.rb",
    "groupTitle": "Item"
  },
  {
    "type": "delete",
    "url": "/todos/:id",
    "title": "Delete a Todo",
    "version": "0.3.0",
    "name": "DeleteTodo",
    "group": "Todo",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>Delete a todo.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhb....Ylg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X DELETE http://localhost:3000/todos/200",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 Success\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can delete the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TodoNotFound",
            "description": "<p>The <code>id</code> of the Todo was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"TodoNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/todos_controller.rb",
    "groupTitle": "Todo"
  },
  {
    "type": "get",
    "url": "/todos/",
    "title": "List all Todos",
    "version": "0.3.0",
    "name": "GetTodo",
    "group": "Todo",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>get details of all todos.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJh.......lg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X GET http://localhost:3000/todos",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Todos-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Todo title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "item",
            "description": "<p>Todo items</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_by",
            "description": "<p>Todo creator</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Todo.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Todo.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/todos_controller.rb",
    "groupTitle": "Todo"
  },
  {
    "type": "get",
    "url": "/todos/:id",
    "title": "Read data of a todo",
    "version": "0.3.0",
    "name": "GetTodo",
    "group": "Todo",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>get details of a todo.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>The Todo-ID.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJh.....lg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X GET http://localhost:3000/todos/200",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Todos-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Todo title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_by",
            "description": "<p>Todo creator</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Todo.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Todo.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TodoNotFound",
            "description": "<p>The <code>id</code> of the Todo was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"TodoNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/todos_controller.rb",
    "groupTitle": "Todo"
  },
  {
    "type": "put",
    "url": "/todos/:id/edit",
    "title": "Update data of a Todo",
    "version": "0.3.0",
    "name": "PutTodo",
    "group": "Todo",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>update details of a Todo.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Todo-title.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -d \"title=changed\" -H \"Authorization: eyJhb....Ylg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X PUT http://localhost:3000/todos/200",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Todos-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Todo title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_by",
            "description": "<p>Todo creator</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Todo.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Todo.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can update the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TodoNotFound",
            "description": "<p>The <code>id</code> of the Todo was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"TodoNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/todos_controller.rb",
    "groupTitle": "Todo"
  },
  {
    "type": "post",
    "url": "/todos/",
    "title": "Create new Todo",
    "version": "0.3.0",
    "name": "postTodo",
    "group": "Todo",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>create new todo.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Todo title</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -d \"title=addtodonew\" -H \"Authorization: eyJh......Ylg3Y\" -H \"Content-Type: application/x-www-form-urlencoded\" -X POST http://localhost:3000/todos",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Todos-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Todo title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_by",
            "description": "<p>Todo creator</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the Todo.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the Todo.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can createcTodo.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid-token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/v1/todos_controller.rb",
    "groupTitle": "Todo"
  },
  {
    "type": "post",
    "url": "/users/",
    "title": "Create new User",
    "version": "0.3.0",
    "name": "postUser",
    "group": "User",
    "permission": [
      {
        "name": "user"
      }
    ],
    "description": "<p>create new user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The User name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The User email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The User password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -d \"name=test&email=test@email.com&password=test&password_confirmation=test\" -H \"Content-Type: application/x-www-form-urlencoded\" -X POST http://localhost:3000/signup",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The User name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The User email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The User password</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n {\n   \"message\": \"Account created successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>failed: Password can't be blank, Name can't be blank, Email can't be blank, Password digest can't be blank.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 422 Unprocessable Entity\n {\n   \"message\": \"Validation failed: Password can't be blank, Password digest can't be blank\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "my-api/app/controllers/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "my-api/public/my-doc/main.js",
    "group": "_home_amira_amira_api_my_api_public_my_doc_main_js",
    "groupTitle": "_home_amira_amira_api_my_api_public_my_doc_main_js",
    "name": ""
  }
] });
